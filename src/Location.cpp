#include <stdio.h>
#include "Location.h"
#include "repast_hpc/AgentId.h"
#include "repast_hpc/SharedDiscreteSpace.h"
#include "repast_hpc/Properties.h"
#include "repast_hpc/Point.h"
#include "repast_hpc/Random.h"
#include <vector>

Location::Location(repast::AgentId FieldID, double soilQual){
	LocationID = FieldID;
	soilQuality = 1 + soilQual;
	isWater = false;
	state = 0;
}

Location::Location(){
	isWater = false;
}

Location::~Location() {}

void Location::setZones(int z, int mz){
	zone = z;
	maizeZone = mz;
}

void Location::setState(int s){
	state = s;
}

//get expected field harvest
int Location::getExpectedYield(){
	return expectedHarvest;
}

double Location::genBenifitProb(double perception, double (&beta)[6], int currentZone, double distance, double historicPdsi){
	//soilQualityX calculations
	double soilQualityX;
	if (soilQuality <= 0.7){
		soilQualityX = 0;
	}
	else if (soilQuality > 0.7 && soilQuality < 1.3){
		soilQualityX = 2*(soilQuality-0.7);
	}
	else{
		soilQualityX = 1.2; // NB currently soil quality is always between 0.7-1.3 99.9% of the time
	}
	//maizeZoneX calculations
	double maizeZoneX;
	if (maizeZone = 4){ // Arable uplands
		maizeZoneX = 0.25;
	}
	else if (maizeZone = 3){ // General Valley
		maizeZoneX = 0.5;
	}
	else if (maizeZone = 2){ // North and Mid Valley, Kinbiko Canyon
		maizeZoneX = 0.75;
	}
	else if (maizeZone = 5){ // Dunes
		maizeZoneX = 1;
	}
	else{
		maizeZoneX = 0; // No Yeild Area
	}
	//interZoneX calculations
	double interZoneX;
	if (currentZone != zone){
		interZoneX = 1;
	}
	else{
		interZoneX = 0;
	}
	//distanceX calculations
	double distanceX;
	if (distance <= 5){
		distanceX = 0;
	}
	else if (distance <= 15){
		distanceX = 0.1*(distance-5);
	}
	else{
		distanceX = 1;
	}
	//pdsiX calculations
	double pdsiX;
	if (historicPdsi <= -3){
		pdsiX = 0;
	}
	else if (historicPdsi <= 2){
		pdsiX = 0.2*(historicPdsi+3);
	}
	else{
		pdsiX = 1;
	}
	double actualBenefitProb = beta[0] + (beta[1] * soilQualityX) + (beta[2] * maizeZoneX) - (beta[3] * interZoneX) - (beta[4] * distanceX) + (beta[5] * pdsiX);
	double percievedBenefitProb = actualBenefitProb * (1+perception);
	//cout<<"soilQualityX: "<<soilQualityX<<" maizeZoneX: "<<maizeZoneX<<" interZoneX: "<<interZoneX<<" distanceX: "<<distanceX<<" pdsiX: "<<pdsiX<<std::endl;
	//cout<<"soilQuality: "<<soilQuality<<" maizeZone: "<<maizeZone<<" zone: "<<zone<<" distance: "<<distance<<" pdsi: "<<historicPdsi<<std::endl;
	//cout<<"perception: "<<perception<<std::endl;
	//cout<<"perceivedBenefit: "<<percievedBenefitProb<<std::endl;
	//cout<<"--"<<std::endl;
	return percievedBenefitProb;
}

//calculate harvest of the field
void Location::calculateYield(int y, double Ha, double gen)
{
	//y is YieldLevel, Ha is Harvest Adjust and q is soil quality
	double baseYield = y * soilQuality * Ha;
	expectedHarvest = baseYield*(1+gen);
}

void Location::checkWater(bool existStreams, bool existAlluvium, int x, int y, int year)
{
	isWater = false;
	for(std::vector<WaterSource>::iterator it = waterSources.begin(); it != waterSources.end(); ++it)
	{
		if(it->waterType == 1)
		{
			if((existAlluvium == 1) && ((zone == 5) or (zone == 4) or (zone == 8) or (zone == 2)))
			{
				isWater = true;
				return;
			}
			else if((existStreams == 1) && (zone == 2))
			{
				isWater = true;
				return;
			}

			//for these locations: (location 72 114) (location 70 113) (location 69 112)	(location 68 111) (location 67 110) (location 66 109) (location 65 108) (location 65 107))
			if (((x==72)&&(y==114))or((x==70)&&(y==113))or((x==69)&&(y==112))or((x==68)&&(y==111))or((x==67)&&(y==110))or((x==66)&&(y==109))or((x==65)&&(y==108))or((x==65)&&(y==107)))
			{
				isWater = true;
				return;
			}
		}
		else if(it->waterType == 2)
		{
			isWater = true;
			return;
		}
		else if(it->waterType == 3)
		{
			if((year >= it->startYear) && (year <= it->endYear))
			{
				isWater = true;
				return;
			}
		}
	}
}

void Location::addWaterSource(int waterType, int startYear, int endYear)
{
	waterSources.push_back({waterType, startYear, endYear});
}
