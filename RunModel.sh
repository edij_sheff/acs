#!/bin/bash
cores=1

Rscript preprocess.R
mpirun -n $cores ./bin/main.exe ./props/config.props ./props/model.props
Rscript postprocess.R
