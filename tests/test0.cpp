#include <boost/mpi.hpp>
#include <stdio.h>
#include "repast_hpc/RepastProcess.h"
#include <stdio.h> 

#include "Model.h"
#include "Location.h"
#include "Household.h"

#include <iomanip>
#include <iostream>
#include <fstream>
#include <vector>

// run with: "mpirun -n 1 tests/test0.exe props/config.props props/model.props"




void test1(AnasaziModel* &test1_model);
void test2(AnasaziModel* &test2_model);
void test3(AnasaziModel* &test3_model);
void test4(AnasaziModel* &test4_model);
void test5(AnasaziModel* &test5_model);
void test6(AnasaziModel* &test6_model);

int main(int argc, char** argv)
{



	std::string configFile = argv[1]; // The name of the configuration file
	std::string propsFile  = argv[2]; // The name of the properties file
	boost::mpi::environment env(argc, argv);
	boost::mpi::communicator* world;

	repast::RepastProcess::init(configFile);
	world = new boost::mpi::communicator;
	AnasaziModel* model = new AnasaziModel(propsFile, argc, argv, world);
	repast::ScheduleRunner& runner = repast::RepastProcess::instance()->getScheduleRunner();

	AnasaziModel* cleanModel = new AnasaziModel(propsFile, argc, argv, world);
 	cleanModel->initAgents();

 	AnasaziModel* cleanModel2 = new AnasaziModel(propsFile, argc, argv, world);
 	cleanModel2->initAgents();

	model->initAgents();
    model->initSchedule(runner);

	runner.run();
 	


  	// run tests here
  	cout<<"\nTest1:\n";
	test1(cleanModel);
	cout<<"\nTest2:\n";
 	test2(cleanModel);
	cout<<"\nTest3:\n";
	test3(cleanModel);
	cout<<"\nTest4:\n";
	test4(model);
	cout<<"\nTest5:\n";
	test5(cleanModel2);
	cout<<"\nTest6:\n";
	test6(cleanModel);

  // when tests are done
	delete model;
	repast::RepastProcess::instance()->done();
}

void test1(AnasaziModel* &test1_model){
     std::cout.flush();
	//Test 1 Insantiate and agent this function checks that the 
	//agents were insantianted and exist 
	Household* test1_agent;
	std::vector<Household*> allAgents;
	test1_model->CheckAgents(&allAgents);
	
	
	if (allAgents.size()==0){
		std::cout << "Test 1 Failed. No agents were insantianted.\n\n"<<std::endl;
		return;
	} else{
		std::cout << "Test 1 Passed " << allAgents.size() << " Agents insantianted. \n"<<std::endl;
	}

}


void test2(AnasaziModel* &test2_model)
{
//Test 2: Update the environment with new archaeological data
	//intialise a households
	Household* test2_household;
	std::vector<Household*> allHousehold;
	// Gets alll of the households that  were already in the model
	test2_model->getHouseholds(&allHousehold);
	//Fails if no households found
	if (allHousehold.size()==0)
	{
		std::cout << "Test 2 Failed. Couldn't find any households.\n\n";
		return;
	}
	//picks one of the house holds to be used for testing
	test2_household = allHousehold[0];
	//get the associated field for household
	Location* test2_location = test2_household->getAssignedField();
	//find the x and y coordinates for
	std::vector<int> xy;
	test2_model->getXY(test2_location->getId(), &xy);
	//Fails if no coordinates found
	if(xy.size()==0)
	{
		std::cout << "Test 2 Failed. Couldn't find coordinates of field.\n\n";
		return;
	}

	// Get data from location from agent being tested
	int State=test2_location->getState();			//state 0-empty 1-household	2-field ***would expect 2
	int Zone=test2_location->getZone();				//Zone Empty-0 Natural-1 Kinbiko-2 Uplands-3 North-4 General-5 North Dunes-6 Mid Dunes-7 Mid-8
	int MaizeZone=test2_location->getMaizeZone();	//MaizeZone Empty-0	No_Yield-1 Yield_1-2||North and Mid Valley, Kinbiko Canyon Yield_2-3||General Valley Yield_3-4||Arable Uplands Sand_dune-5||Dunes
	//Print Data found
	std::cout << "X:" << xy[0] << " Y:"<< xy[1] << " State(1-Hhld 2-Fld):" << State << " Zone:" << Zone << " Maize_Zone:" << MaizeZone << "\n";

	//read from map csv to check data
	int x,y,z , mz;
	string foundZone, foundMaizeZone, temp;
	bool found = false;
	std::ifstream file ("data/map.csv");//define file object and open map.csv
	file.ignore(500,'\n');//Ignore first line

	//read from  file until coordinates found
	while(!found)
	{
		getline(file,temp,','); 		//check if at end of file
		if(!temp.empty())
		{
			x = repast::strToInt(temp); //Read until ',' and convert to int & store in x
			getline(file,temp,',');
			y = repast::strToInt(temp); //Read until ',' and convert to int & store in y
			//if finds correct coordinates write data from row
			if (x==xy[0]&&y==xy[1])
			{
				found = true;
				getline(file,temp,','); 			// colour not used in the file
				getline(file,foundZone,',');		// read until ',' and store into zone
				getline(file,foundMaizeZone,'\n');	// read until next line and store into maizeZone
				// Convert zones from string to int to compare with values in model
				if(foundZone == "\"Empty\""){ z = 0; }
				else if(foundZone == "\"Natural\""){z = 1;}
				else if(foundZone == "\"Kinbiko\""){z = 2;}
				else if(foundZone == "\"Uplands\""){z = 3;}
				else if(foundZone == "\"North\""){z = 4;}
				else if(foundZone == "\"General\""){z = 5;}
				else if(foundZone == "\"North Dunes\""){z = 6;}
				else if(foundZone == "\"Mid Dunes\""){z = 7;}
				else if(foundZone == "\"Mid\""){z = 8;}
				else{z = 99;}
				// Convert maize zones from string to int to compare with values in model
				if(foundMaizeZone.find("Empty") != std::string::npos){mz = 0;}
				else if(foundMaizeZone.find("No_Yield") != std::string::npos){mz = 1;}
				else if(foundMaizeZone.find("Yield_1") != std::string::npos){mz = 2;}
				else if(foundMaizeZone.find("Yield_2") != std::string::npos){mz = 3;}
				else if(foundMaizeZone.find("Yield_3") != std::string::npos){mz = 4;}
				else if(foundMaizeZone.find("Sand_dune") != std::string::npos){mz = 5;}
				else{mz = 99;}
			}
			//read lines and disgard until coordinates found
			else
			{
				getline(file,temp,'\n');
			}
		}
		else
		{
			found=true;
			cout<<"Test2 Failed coordinates not found in map.csv\n\n";
			return;
		}
	}
	//Print data from csv
	std::cout << "X:" << xy[0] << " Y:"<< xy[1] << " Zone:" << Zone << " Maize_Zone:" << MaizeZone << "\n";
	//compare data from model with that in the csv
	if(Zone==z&&MaizeZone==mz)
	{
		std::cout << "Test 2 Passed\n\n";
	}
	else
	{
		std::cout << "Test 2 Failed didn't match\n\n";
	}
}

void test3(AnasaziModel* &test3_model)
{
	int min_death_age, max_death_age;
	double mean_death_age;

	tie(mean_death_age, min_death_age, max_death_age) = test3_model->getMeanDeathAge();
	if (min_death_age < mean_death_age && mean_death_age < max_death_age)
		{
			std::cout << "Mean death age is " << mean_death_age << "\n";
			std::cout << "Test 3 Passed\n\n";
		}
	else
		{
			std::cout << "Mean death age is " << mean_death_age << "\n";
			std::cout << "Mean death age is " << min_death_age << "\n";
			std::cout << "Mean death age is " << max_death_age << "\n";
			std::cout << "Test 3 Failed (mean death age not in expected range)\n\n";
		}
}

void test4(AnasaziModel* &test4_model){
    Household* test4_household;
	std::vector<Household*> allHousehold;
	
	std::ifstream file ("NumberOfHousehold.csv");//define file object and open map.csv
	
	//file.ignore(500,'\n');//Ignore first line
    int year,NoHouse;
    std::vector<int> AllYears;
    std::vector<int> Pop;
    string temp;
	//read from  file 
	if (!file.is_open()) {
	// check for successful opening
		cout << "Input file could not be opened! Terminating!" << endl;
		return;
	}
	file.ignore(500,'\n');//Ignore first line
	while(file){
	   
		
		getline(file,temp,','); //getline(file,temp,',');
		if(temp.empty()){
			std::cout<<"end of file"<<std::endl;
		}		//check if at end of file
		if(!temp.empty()){
			year= repast::strToInt(temp);
			AllYears.push_back(year); //Read until ',' and convert to int & store in year
			getline(file,temp);
			NoHouse= repast::strToInt(temp); //Read until ',' and convert to int & store in NoHouse
			//if finds correct coordinates write data from row
            Pop.push_back(NoHouse);
           // std::cout << "File read pop simulation " << NoHouse << "\n"<<std::endl;
         //   std::cout << "File read year simulation " << year << "\n"<<std::endl;
} //was 
		}
	
file.close();

// Check accuaracy with regards to simulation 
std::ifstream Simfile ("data/target_data.csv");//define file object and open map.csv
	
	//file.ignore(500,'\n');//Ignore first line
    int Simyear,SimNoHouse;
    std::vector<int> SimYears;
    std::vector<int> SimPop;
    string filein;
	//read from  file 
	if (!Simfile.is_open()) {
	// check for successful opening
		cout << "Input file could not be opened! Terminating!" << endl;
		return;
	}
	if(Simfile.fail()){
		cout<<"Error"<<endl;
	}
	//Simfile.ignore(500,'\n');//Ignore first line
	while(Simfile){
	   
		
		getline(Simfile,filein,','); //getline(file,temp,',');
		if(filein.empty()){
			std::cout<<"end of file "<<filein<<std::endl;
            continue;
            getline(Simfile,filein); 
		}		//check if at end of file
		if(!filein.empty()){
			
			Simyear= repast::strToInt(filein);
			SimYears.push_back(year); //Read until ',' and convert to int & store in year
			getline(Simfile,filein);
			SimNoHouse= repast::strToInt(filein); //Read until ',' and convert to int & store in NoHouse
			//if finds correct coordinates write data from row
            SimPop.push_back(SimNoHouse);
         //   std::cout << "File read pop  " << SimNoHouse << "\n"<<std::endl;
          //  std::cout << "File read year " << Simyear << "\n"<<std::endl;
} //was temp
		
	}
	
	
Simfile.close();

int i;//iterate over the first 5 elements of the vector 
for(i=0;i<6;i++){
std::cout<<"Target population was "<<SimPop[i]<<std::endl;
std::cout<<"Simulated population was "<<Pop[i]<<std::endl;
}



return;
}

void test5(AnasaziModel* &test5_model)
{
//Test 5: Manipulate an agent’s field cell so that it will not provide sufficient maize for an agent. Check for the intended behaviour of the agent.
	//Intialise household to get agent from model
	Household* test5_household;
	std::vector<Household*> allHousehold;
	//get all households from model
	test5_model->getHouseholds(&allHousehold);
	//fail test if no households can be found
	if(allHousehold.size()==0)
	{
		std::cout << "Test 5 failed no households found\n\n";
		return;
	}
	//select household from model to be used
	test5_household = allHousehold[0];
	//Get the field agent attached to the Household
	Location* test5_location = test5_household->getAssignedField();
	std::vector<int> xy;
	//find coordinates of the field
	test5_model->getXY(test5_location->getId(), &xy);
	//Fails if no coordinates found
	if(xy.size()==0)
	{
		std::cout << "Test 5 Failed. Couldn't find coordinates of field.\n\n";
		return;
	}
	//print coordinates and expected yeild
	std::cout << "Coordinates of field: " << xy[0] <<","<< xy[1] << "  Expected yeild of field:" << test5_location->getExpectedYield() << '\n';
	//set zone to empty and no yield
	test5_location->setZones(0,0);
	test5_location->calculateYield(0,0,0);

	//run model for 1 iteration
	repast::ScheduleRunner& runner1 = repast::RepastProcess::instance()->getScheduleRunner();
	test5_model->initScheduleRun1(runner1);
	runner1.run();

	//get new field location
	Location* test5_location2 = test5_household->getAssignedField();
	std::vector<int> xy2;
	//find coordinates of the field
	test5_model->getXY(test5_location2->getId(), &xy2);
	//Fails if no coordinates found
	if(xy2.size()==0)
	{
		std::cout << "Test 5 Failed. Couldn't find coordinates of new field.\n\n";
		return;
	}
	//print coordinates and expected yeild
	std::cout << "Coordinates of new field: " << xy2[0] <<","<< xy2[1] << "  Expected yeild of field:" << test5_location2->getExpectedYield() << '\n';
	//Print message if the household moved field or not
	if (xy[0]==xy2[0] && xy[1]==xy2[1])
	{
		std::cout << "Test 5 Failed. Household didn't move\n\n";
	}
	else
	{
		std::cout << "Test 5 Passed\n\n";
	}

}

void test6(AnasaziModel* &test6_model)
{
//Trigger an agent to move its residence. Confirm that the new location complies with the rules for residence selection
	bool fail,failA,failC = false;
	Household* test6_household;
	std::vector<Household*> allHousehold;
	//get all households from model
	test6_model->getHouseholds(&allHousehold);
	//fail test if no households can be found
	if(allHousehold.size()==0)
	{
		std::cout << "Test 6 failed no households found\n\n";
		fail = true;
		return;
	}
	//select household from model to be used
	test6_household = allHousehold[0];
	if (test6_model->relocateHousehold(test6_household)==false)
	{
		std::cout << "Test 6 failed unable to relocate household\n\n";
		fail = true;
		return;
	}
	//gets new household location 
	Location* test6_houseLoc = test6_model->getHouseholdLocation(test6_household);
	Location* test6_fieldLoc = test6_household->getAssignedField();
	std::vector<int> houseXY,fieldXY;
	test6_model->getXY(test6_houseLoc->getId(), &houseXY);
	test6_model->getXY(test6_fieldLoc->getId(), &fieldXY);
	float dist =sqrt(pow(houseXY[0]-fieldXY[0],2)+pow(houseXY[1]-fieldXY[1],2));
	if(dist>10)
	{
		std::cout << "Failed check field and house greater than 1km away are " << dist/10 <<"km away.\n";
		failA = true;
	}
	if(test6_houseLoc->getState()==2)
	{
		std::cout << "Test 6 Failed household located in field\n";
		fail = true;
	}
	if(test6_houseLoc->getExpectedYield()>test6_fieldLoc->getExpectedYield())
	{
		std::cout << "Failed check household has greater expected yield than field\n"<<"Field yield:"<<test6_fieldLoc->getExpectedYield()<<"  House yield:"<<test6_houseLoc->getExpectedYield()<<"\n\n";
		failC = true;
	}
	if(failA==true||failC==true){
		std::cout << "Test 6 Passed. Allowed to fail those conditions if no better candidate\n\n";
	}
	else if(fail==false){
		std::cout << "Test 6 Passed\n\n";
	}
}