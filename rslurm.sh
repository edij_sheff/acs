#!/bin/bash
#SBATCH --comment=g3_initial_test
#SBATCH —mem=1000
#SBATCH —time=01:00:00
#SBATCH --mail-user=eireland-jones1@sheffield.ac.uk
#SBATCH --array=0-20

# line below must be commented to run locally
module load apps/R/3.6.1 # Load version 3.6.1 of R

# line below must be commented to run on the hpc
#SLURM_ARRAY_TASK_ID=111

R CMD BATCH --no-save --no-restore --args -${SLURM_ARRAY_TASK_ID} WrapperCode.R outputlog.R.o${SLURM_ARRAY_TASK_ID}


#R CMD BATCH --no-save --no-restore WrapperCode.R
