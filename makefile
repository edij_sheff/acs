include ./env

.PHONY: create_folders
create_folders:
	mkdir -p objects
	mkdir -p bin
	mkdir -p tests

.PHONY: clean_compiled_files
clean_compiled_files:
	rm -f ./objects/*.o
	rm -f ./bin/*.exe

.PHONY: clean
clean:
	rm -rf objects
	rm -rf bin

.PHONY: compile
compile: clean_compiled_files
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -g -I./include -c ./src/Main.cpp -o ./objects/Main.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -g -I./include -c ./src/Model.cpp -o ./objects/Model.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -g -I./include -c ./src/Household.cpp -o ./objects/Household.o
	$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -g -I./include -c ./src/Location.cpp -o ./objects/Location.o
	$(MPICXX) $(BOOST_LIB_DIR) $(REPAST_HPC_LIB_DIR) -g -o ./bin/main.exe  ./objects/Main.o ./objects/Model.o ./objects/Household.o ./objects/Location.o $(REPAST_HPC_LIB) $(BOOST_LIBS)

.PHONY: all
all: clean create_folders compile compile_tests

.PHONY: clean_compiled_test_files
	rm -f ./tests/*.o
	rm -f ./tests/*.exe

.PHONY: compile_tests
compile_tests: clean_compiled_test_files
		$(MPICXX) $(REPAST_HPC_DEFINES) $(BOOST_INCLUDE) $(REPAST_HPC_INCLUDE) -g -I./include -c ./tests/test0.cpp -o ./tests/Test0.o
		$(MPICXX) $(BOOST_LIB_DIR) $(REPAST_HPC_LIB_DIR) -g -o ./tests/test0.exe  ./tests/Test0.o ./objects/Model.o ./objects/Household.o ./objects/Location.o $(REPAST_HPC_LIB) $(BOOST_LIBS)
